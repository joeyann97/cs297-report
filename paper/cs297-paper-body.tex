\section{Introduction}
The application of insurance in modern society is based on a chain of trust between clients and insurers built upon an intangible promise to pay. It is usually achieved through a contract or agreement between consenting parties regarding timely exchange of payment related to the client's insured assets. The confidence of clients in an insurer is an important aspect of the  insurer's business value, and it is greatly affected by any apparent risks that could compromise their agreement and consequently, weaken the chain of trust. Conversely, insurers also face a growing threat from fraud, whether from small-claims fraud by individuals or from organized fraud consisting of multiple parties. \cite{Crawford2016, Shelkovnikov2016}

Blockchain is a novel technology that is believed to be able to transform the insurance industry, among other finance industries. A blockchain represents an immutable, distributed, append-only ledger of transactions, requiring no central authority in operation. The trustless system that it enables provides long-term strategic benefits for insurers, in that it could provide clients and insurers with the means to digitally manage claims in a transparent and irrefutable manner. \cite{Crawford2016, Shelkovnikov2016, Kshetri2017}

Particularly, in healthcare, we deal with insurance claims as a mode of payment for medical procedures. By nature, the processing of such a payment is less straightforward than other classic means such as cash, as it involves interactions between more entities: the hospitals or healthcare providers (the payees), the insurers (the payers), and the patients (the insurance clients). In this network of entities lie interactions dictated by the insurance contracts and the providers' and insurers' respective workflows. \cite{Engelhardt2017, Myburgh2018}

Blockchain is ideal to use when: \cite{Engelhardt2017}
\begin{enumerate}[topsep=0pt]
	\item There are multiple stakeholders involved.
	\item More trust is required between the stakeholders.
	\item An intermediary can be removed to increase trust or efficiency.
	\item There is a need for reliable tracking of activity.
	\item There is a need for data to be reliable over time.
\end{enumerate}
We see that these conditions are manifested in healthcare insurance management. The trust that the insurer will pay is held between the multiple entities involved. The replacement of a manual intermediary that verifies the claims as they are submitted by automation will speed up the process. An immutable record of activity will aid in the audit of insurance transactions, an important aspect of detecting fraudulent claims.

In this paper, we present a proof of concept in the automation of healthcare insurance claims processing using blockchain. Our proof of concept is based on the suggestion of Dr. Alvin Marcelo of the Philippine General Hospital citing healthcare insurance claims processing as one of the promising use cases of blockchain for the institution.

\section{Related work}
Peterson et al. \cite{Peterson2016}, MeDShare by Xia et al. \cite{Xia2017}, and MedRec by Azaria et al. \cite{Azaria2016} address the problem of securely sharing healthcare information between different institutions using blockchain.

Peterson et al. \cite{Peterson2016} use the blockchain as an immutable ledger for medical records using references to off-chain storage which contain the actual patient records. This approach keeps sensitive patient data off-chain as only the references to the actual payloads and their hashes, which are used to verify the integrity of the resources referenced, are stored on the chain. In particular, Fast Healthcare Interoperability Resources (FHIR) URLs are used as the contents of blockchain transactions. FHIR is an emerging standard depicting data formats for the purpose of exchanging electronic health records.

MedRec \cite{Azaria2016} and MeDShare \cite{Xia2017}, on the other hand, both use smart contracts in their respective approaches to sharing of healthcare information. The main use of the blockchain in both these works is to hold information regarding ownership and access permissions of records. Specifically, smart contracts are used to store and monitor the changes to these record ownership and access information, while the patient data are stored in external databases. This approach enables the patient to be more involved in controlling the access to their medical information.

MIStore by Zhou et al. \cite{Zhou2018} deals with a similar task as ours and details a system of interaction between patients, hospitals, insurers, and a total of $n$ servers to be used for a ($t$, $n$)-threshold distributed protocol. One of the preliminaries of their protocol is that all parties mortgage an amount of guarantee coins to a smart contract. If any one of the entities publish incorrect data in the blockchain, any other entity can input the evidences into the contract to obtain part that entity's guarantee deposit. All processes in the system are recorded in the blockchain through transactions, which come in four types: initialize, record, query, and respond transactions. The role of the hospital is to first create an initialize-transaction which contains the security keys and information that will be used by each server in the distributed protocol, each of which are encrypted using the corresponding server's public key. After initialization, the hospital can now broadcast, using record-transactions, the patient's medical spending data to the $n$ servers which will store and protect it. Using the ($t$, $n$)-threshold protocol, an insurer can create a query-transaction to request for a patient's spending data. It obtains the result when no less than $t$ out of $n$ servers provide correct responses via a response-transaction. The insurer can infer nothing about the spending data if there are less than $t$ servers that give the correct response. Likewise, the $n$ servers can also infer nothing about the patient's spending data so long as there are more than $n - t$ honest servers.


\section{Statement of the problem}
The succeeding sections of this paper detail a blockchain-based approach for healthcare insurance claims verification in the payment process for medical procedures at the Philippine General Hospital. In particular, our proposed solution will cover the following steps in healthcare insurance claims processing:
\begin{enumerate}
	\item Insurer workflow:
		\begin{enumerate}
			\item Creation of policies for each person to be insured
			\item Addition of new procedures to be included to the policies of the insured.
			\item Deduction of the cost of the procedures done on the insuree's remaining cost coverage.
		\end{enumerate} 
	\item Hospital/healthcare provider workflow:
		\begin{enumerate}
			\item Verification of the existence of a patient's insurance policy.
			\item Verification that a procedure is covered by the patient's insurance policy.
			\item Billing of a patients procedure(s) to the insurer.
		\end{enumerate}
\end{enumerate}


\section{System design}
\subsection{Smart contracts}
Our proposed implementation to the task of healthcare insurance claims verification employs blockchain-based smart contracts. A \textit{smart contract} is a tamper-proof application that autonomously executes code when some predefined states or conditions are observed.

\subsection{Data structures and related methods} \label{DataStructures}
Figure \ref{fig:classes} shows the data structures used in the smart contract. Public class methods in blue are intended to be used by the insurers, while methods in green are for the use of hospitals. Those in black can be used by both entities.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{images/classes}
	\caption{Smart contract data structures}
	\label{fig:classes}
\end{figure}

\texttt{Patient} and \texttt{Policy} are minimal abstractions for patients and policies, respectively. $Policy$ contains the funds, name of the insuree, and the insured processes. $Patient$ contains the name of the patient and the processes done on the patient.

\texttt{InsurancePolicy} is the main contract and is responsible for the creation and validation of policies for both the insurer and hospital. Its member variables and their roles are as follows:
\begin{enumerate}
 	\item $hospital$ and $insurer$. The Ethereum addresses of the hospital and the insurer, respectively. These will be used to verify if the caller of the functions in the smart contract are the correct ones.
 	\item $patients$. A list of patients whose information are contained in the contract.
 	\item $policies$. A list of policies that detail procedures covered by the insurer for insurees.
 	\item $response\_to\_hospital$ and $response\_to\_insurer$. The most recent response of the contract to the hospital and insurer, respectively.
\end{enumerate}

Its member functions and their roles are as follows:
\begin{enumerate}
	\item \texttt{addPolicy}. This function is used to create a new policy for a person. 
	\item \texttt{addProcedure}. This function can: (a) add a procedure done to a patient if it used by the hospital and (b) add a new procedure to be insured if it is used by the insurer.
	\item \texttt{getResponse}. This function is used by the user interface in displaying the results of the most recent transaction executed.
	\item \texttt{validatePatient}. This function is used by the hospital to determine if a person really has insurance.
	\item \texttt{admitPatient}. This function is used by the hospital to admit a patient.
	\item \texttt{deductFromFunds}. This function is used by the hospital to start deducting funds from the patient's insurance funds if the process is insured. Uses \texttt{\textit{checkIfProcedureIsInsured}}.
	\item \texttt{checkIfProcedureIsInsured}. This function is private and is used by \texttt{\textit{deductFromFunds}} function to check if a process done to a patient is insured.
\end{enumerate}

\subsection{Interactions}
Figure \ref{fig:interactionins} depicts the insurer's interactions with the contract.
\begin{figure}[h]
	\centering
	\includegraphics[width=1.2\linewidth]{images/seqdiagins}
	\caption{Insurer's interactions with the contract}
	\label{fig:interactionins}
\end{figure}

Figure \ref{fig:interactionhosp} depicts the hospital's interactions with the contract.
\begin{figure}[h]
	\begin{adjustwidth}{-0.5in}{0in}
	\centering
	\includegraphics[width=1\linewidth]{images/seqdiaghosp}
	\end{adjustwidth}
	\caption{Hospital's interactions with the contract}
	\label{fig:interactionhosp}
\end{figure}

\section{Implementation}
\subsection{Ethereum and Solidity}
Ethereum is one of the first and most widely used smart contract platforms to date. It builds into the blockchain a Turing-complete instruction set to allow highly programmable smart contracts and a storage capability to accommodate on-chain state. Smart contracts in Ethereum are implemented using the object-oriented language, Solidity. We use this in our implementation along with their endorsed IDE, Remix, for compilation of the code.

\subsection{The InsurancePolicy contract}
The variables needed in the smart contract are shown below in their Solidity implementation as they are described in \ref{DataStructures}. Particularly, the variables $response\_to\_hospital$ and $response\_to\_insurer$ are necessary because in Ethereum, functions that change the blockchain cannot return values. This means that functions that change the blockchain will not be able to send success or fail messages to the sender. These types of messages will instead be stored in these variables. The user will just call a getter function to these variables to get this message.
\lstinputlisting[firstline=4,lastline=22,language=C, caption=Variables of the smart contract]{../code/InsurancePolicy.sol}

The \texttt{addPolicy} function accepts three parameters: the funds, the name of the insuree, and the list of insured process. To successfully add a policy, the sender must be the insurer and the patient should not have an existing policy.
\lstinputlisting[firstline=32,lastline=57, caption=Add policy function]{../code/InsurancePolicy.sol}

The \texttt{admitPatient} function accepts a patient name as parameter. To admit a patient successfully, the sender must be the hospital and the patient should not be in the $patients$ array.
\lstinputlisting[firstline=77,lastline=99, caption=Admit patient function]{../code/InsurancePolicy.sol}

The \texttt{validatePatient} function accepts only a patient or insuree name as parameter. The function first checks if the user is the hostipal, then it checks whether the patient has an existing policy.
\lstinputlisting[firstline=59,lastline=75, caption=Validate patient function]{../code/InsurancePolicy.sol}

The \texttt{addProcedure} function accepts the name of the patient or insuree and the procedure to be added. The function checks whether the sender is the hospital or the insurer. If the sender is the hospital, the procedure will be added to the patient record. If the sender is the insurer, then the procedure will be added to the insured processes of the patient. 
\lstinputlisting[firstline=109,lastline=150, caption=Add procedure function]{../code/InsurancePolicy.sol}

The \texttt{deductFromFunds} function accepts three parameters: the cost to be deducted, the name of the patient, and the procedure done on the patient. Before deducting the funds, the function checks for 3 things. The function first checks if the patient exists. Then the function checks if the process done on the patient is insured. Finally, the function checks whether the patient has enough funds. If any of these checks return negative, the deduction will not push through, and the hospital is informed via the return message.
\lstinputlisting[firstline=166,lastline=197, caption=Deduct from funds function]{../code/InsurancePolicy.sol}

\subsection{Contacting the contract}
To be able to communicate with the smart contract, the \texttt{web3.js} library is used. Web3.js is an API for interacting with the Ethereum blockchain \cite{Web3}. With this library, a web page can be created to communicate with a smart contract running in the Ethereum blockchain using Javascript.

The code below shows how to connect to a contract in Javascript which is explained in \cite{Web3}. There are two ways to call a function in the smart contract: 
\newline \texttt{\scriptsize{InsurancePolicy.functionName(param1 [, ...])}} or 
\newline \texttt{\scriptsize{InsurancePolicy.functionName.call(param1 [, ...])}}.
\newline
The first is used when the function does not have a return value and the second one is used when the function has a return value.
\lstinputlisting[firstline=1,lastline=10, caption=Contacting the contract using web3.js]{../code/"insurance blockchain"/connect-template.js }

TestRPC is an Ethereum client which is used to test and develop smart contracts \cite{Web3}. When TestRPC runs, it generates 10 addresses or accounts that the developer can use to contact the smart contract. In this study, the first address provided by TestRPC is used as the address of the hospital and the second address is used as the address of the insurer. These are denoted by $web3.eth.accounts[0]$ and $web3.eth.accounts[1]$, respectively.

The codes below are the javascript code for calling the smart contracts. The flow of the code is generally the same. First, the input of the user in the HTML is retrieved. Then, the corresponding function is called. Finally, the response of the smart contract is retrieved by calling the $getResponse()$ function.
\lstinputlisting[firstline=54,lastline=67, caption=Javascript code for add policy]{../code/"insurance blockchain"/insurer-add-policy.html}
\lstinputlisting[firstline=27,lastline=32, caption=Javascript code for admit patient]{../code/"insurance blockchain"/hospital-admit-patient.html}
\lstinputlisting[firstline=32,lastline=40, caption=Javascript code for deduct funds]{../code/"insurance blockchain"/hospital-deduct-funds.html}
\lstinputlisting[firstline=28,lastline=36, caption=Javascript code for validate patient]{../code/"insurance blockchain"/hospital-validate-patient.html}
\lstinputlisting[firstline=32,lastline=48, caption=Javascript code for add procedure]{../code/"insurance blockchain"/add-procedure.html}

\section{Results}
We have created a web page for each of the public functions in the smart contract. Each web page contains a text box for each of the parameters required for the smart contract function. Each web page also contain a button that calls the corresponding smart contract function. The response of the smart contract is displayed below the button.

Figure \ref{fig:add-policy-1} shows the user interface for adding a policy. The webpage has an + and - button which is used if the user wants to add or delete an insured process, respectively.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.85\linewidth]{images/add-policy-1}
	\caption{Adding a policy successfully}
	\label{fig:add-policy-1}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.85\linewidth]{images/add-policy-2}
	\captionsetup{justification=centering}
	\caption{Adding a policy to a patient with an existing policy}
	\captionsetup{justification=centering}
	\label{fig:add-policy-2}
\end{figure}

The web page for adding a procedure contains a selection box where the user can choose between hospital and insurer. If hospital is chosen, then the procedure will be added to the patient's record. If insurer is chosen, then the procedure will be added to the insured processes of the patient.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{images/add-procedure-1}
	\captionsetup{justification=centering}
	\caption{Adding a procedure to a patient}
	\label{fig:add-procedure-1}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{images/add-procedure-2}
	\captionsetup{justification=centering}
	\caption{Adding an insured procedure to a patient}
	\label{fig:add-procedure-2}
\end{figure}

Figures \ref{fig:admit-patient-1} to \ref{fig:deduct-funds-3} shows the user interfaces for the other functions.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{images/admit-patient-1}
	\caption{Admitting a patient succesfully}	
	\label{fig:admit-patient-1}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{images/admit-patient-2}
	\captionsetup{justification=centering}
	\caption{Admitting a patient that is already admitted}
	\label{fig:admit-patient-2}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{images/validate-patient-1}
	\caption{Patient is validated}	
	\label{fig:validate-patient-1}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{images/validate-patient-2}
	\caption{Patient is not validated}	
	\label{fig:validate-patient-2}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{images/deduct-funds-1}
	\captionsetup{justification=centering}
	\caption{Deducting funds from a user}
	\label{fig:deduct-funds-1}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{images/deduct-funds-2}
	\captionsetup{justification=centering}
	\caption{Funds is not enough}
	\label{fig:deduct-funds-2}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\linewidth]{images/deduct-funds-3}
	\captionsetup{justification=centering}
	\caption{Process is not insured}
	\label{fig:deduct-funds-3}
\end{figure}
\section{Conclusions and future work}
In this paper, we have shown how to use a blockchain for healthcare insurance claims processing. Specifically, we have created a smart contract that automates the insurance process. Since the smart contract runs in the blockchain, we are assured that the automation process cannot be altered and it runs as intended.

One of the things that needs to be done is to improve the privacy property. One limitation of the solution we provided is privacy. The Philippines has a law regarding data privacy \cite{DataPrivacy}. Using a blockchain might violate this law since patient records or data are stored in the blockchain. One solution to this is to store an identifier for the patient and not to use the patient's name directly. Using this method might not always provide privacy since people can infer patterns from the blockchain \cite{Azaria2016}. We can also improve on allowing multiple hospitals to interact with the system, which should create more incentive for insurers to use a blockchain-based system for claims management.