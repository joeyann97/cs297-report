if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
	// set the provider you want from Web3.providers
	web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
}

var InsurancePolicyContract = web3.eth.contract([
	{
		"constant": true,
		"inputs": [
			{
				"name": "insuree",
				"type": "bytes32"
			}
		],
		"name": "validatePatient",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "cost",
				"type": "int256"
			},
			{
				"name": "name",
				"type": "bytes32"
			},
			{
				"name": "procedure",
				"type": "bytes32"
			}
		],
		"name": "deductFromFunds",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "user",
				"type": "bool"
			}
		],
		"name": "getResponse",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "fund",
				"type": "int256"
			},
			{
				"name": "insuree",
				"type": "bytes32"
			},
			{
				"name": "insuredProcesses",
				"type": "bytes32[]"
			}
		],
		"name": "addPolicy",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "procedure",
				"type": "bytes32"
			},
			{
				"name": "name",
				"type": "bytes32"
			}
		],
		"name": "addProcedure",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "patientName",
				"type": "bytes32"
			}
		],
		"name": "admitPatient",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"name": "_hospital",
				"type": "address"
			},
			{
				"name": "_insurer",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	}
]);

var InsurancePolicy = InsurancePolicyContract.at('0xfc38489b3017c8b07aeac4fe85f3662908393cb3');