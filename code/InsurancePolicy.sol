pragma solidity >=0.4.22 <0.6.0;
contract InsurancePolicy
{
struct Policy
{
    int fund;
    bytes32 insuree;
    bytes32[] insuredProcesses;
}

struct Patient
{
    bytes32 patientName;
    bytes32[] processesDone;
}

address hospital;
address insurer;
Policy[] policies;
Patient[] patients;
string response_to_hospital = "";
string response_to_insurer = "";

constructor(address _hospital, address _insurer) public
{
    hospital = _hospital;
    insurer = _insurer;
    policies.length = 0;
    patients.length = 0;
}

function addPolicy(int fund, bytes32 insuree, bytes32[] memory insuredProcesses) public
{
    if (msg.sender != insurer)
    {
        response_to_insurer = "Account must be insurer.";
        return;
    }

    uint policiesSize = policies.length;
    for (uint i = 0; i < policiesSize; i++)
    {
        if(policies[i].insuree == insuree)
        {
            response_to_insurer = "Insuree already has a policy.";
            return;
        }
    }

    Policy memory policy;
    policy.fund = fund;
    policy.insuree = insuree;
    policy.insuredProcesses = insuredProcesses;
    policies.push(policy);
    response_to_insurer = "Policy successfully added.";

}

function validatePatient(bytes32 insuree) public view returns(bool)
{
    if (msg.sender != hospital)
    {
        return false;
    }
    uint policiesSize = policies.length;
    for (uint i = 0; i < policiesSize; i++)
    {
        if(policies[i].insuree == insuree)
        {
            return true;
        }
    }

    return false;
}

function admitPatient(bytes32 patientName) public
{
    if (msg.sender != hospital)
    {
        response_to_hospital = "Account must be hospital.";
        return;
    }

    uint patientsSize = patients.length;
    for (uint i = 0; i < patientsSize; i++)
    {
        if(patients[i].patientName == patientName)
        {
            response_to_hospital = "Patient already exists.";
            return;
        }
    }

    Patient memory patient;
    patient.patientName = patientName;
    patients.push(patient);
    response_to_hospital = "Patient successfully added.";
}

//Gets the last response of a function. user = 1 means hospital, else insuree
function getResponse(bool user) public view returns(string memory)
{
    if(user)
        return response_to_hospital;
    return response_to_insurer;
}

function addProcedure(bytes32 procedure, bytes32 name) public
{
    if (msg.sender != hospital && msg.sender != insurer)
    {
        response_to_insurer = response_to_hospital = "Account should be a hospital or insurer.";
    }

    if (msg.sender == hospital)
    {
        uint patientsSize = patients.length;
        for (uint i = 0; i < patientsSize; i++)
        {
            if(patients[i].patientName == name)
            {
                patients[i].processesDone.push(procedure);
                response_to_hospital = "Process added to patient.";
                return;
            }
        }
        response_to_hospital = "Patient does not exist.";
        return;
    }

    if (msg.sender == insurer)
    {
        uint policiesSize = policies.length;
        for (uint i = 0; i < policiesSize; i++)
        {
            if(policies[i].insuree == name)
            {
                policies[i].insuredProcesses.push(procedure);
                response_to_insurer = "Process added to patient insurance.";
                return;
            }
        }
        response_to_insurer = "Insuree does not exist";
        return;

    }
    response_to_hospital = response_to_insurer = "Process was not added successfully";
    return;
}

function checkIfProcedureIsInsured(bytes32 procedure,  bytes32[] memory insuredProcesses) private pure returns(bool)
{
    uint insuredProcessesSize = insuredProcesses.length;
    for (uint i = 0; i < insuredProcessesSize; i++)
    {
        if(insuredProcesses[i] == procedure)
        {
            return true;
        }
    }

    return false;
}

function deductFromFunds(int cost, bytes32 name, bytes32 procedure) public
{
    if (msg.sender != hospital)
    {
        response_to_hospital = "Account should be hospital.";
        return;
    }
    bool isInsured = false;
    uint policiesSize = policies.length;
    for (uint i = 0; i < policiesSize; i++)
    {
        if(policies[i].insuree == name)
        {
            isInsured = checkIfProcedureIsInsured(procedure, policies[i].insuredProcesses);
            if (!isInsured)
            {
                response_to_hospital = "Procedure does not exist.";
                return;
            }
            if(cost > policies[i].fund)
            {
                response_to_hospital = "The funds is not enough.";
                return;
            }
            policies[i].fund -= cost;
            response_to_hospital = "The funds was successfully deducted.";
            return;
        }
    }
    response_to_hospital = "The patient does not exist.";
    return;
}
}